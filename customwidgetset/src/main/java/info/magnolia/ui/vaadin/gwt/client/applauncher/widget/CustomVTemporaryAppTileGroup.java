package info.magnolia.ui.vaadin.gwt.client.applauncher.widget;

import info.magnolia.ui.vaadin.gwt.client.applauncher.connector.CustomAppLauncherConnector;
import info.magnolia.ui.vaadin.gwt.client.jquerywrapper.AnimationSettings;
import info.magnolia.ui.vaadin.gwt.client.jquerywrapper.JQueryCallback;
import info.magnolia.ui.vaadin.gwt.client.jquerywrapper.JQueryWrapper;

/**
 * Created by tuan on 5/11/17.
 * Copy from {@link VTemporaryAppTileGroup} version 5.4.8
 */
public class CustomVTemporaryAppTileGroup extends VAppTileGroup {

    private static final int OPEN_STATE_HEIGHT_PX = 90;
    private static final int VISIBILITY_TOGGLE_SPEED = 200;

    private JQueryCallback layoutRequestCallback = new JQueryCallback() {
        @Override
        public void execute(JQueryWrapper query) {
            CustomAppLauncherConnector connector = CustomAppLauncherUtil.getConnector();
            connector.getLayoutManager().setNeedsMeasure(connector);
            connector.getLayoutManager().layoutNow();
        }
    };

    public CustomVTemporaryAppTileGroup(String color) {
        super(color);
        construct();
    }

    @Override
    protected void construct() {
        addStyleName("temporary");
        closeSection();
    }

    public void closeSection() {
        final AnimationSettings settings = new AnimationSettings();
        settings.setProperty("height", 0);
        settings.addCallback(layoutRequestCallback);

        JQueryWrapper.select(this).animate(VISIBILITY_TOGGLE_SPEED, settings);
    }

    public void showSection() {
        int iRows = 1 + (getChildren().size() - 1) / 9;

        final AnimationSettings settings = new AnimationSettings();
        settings.setProperty("height", OPEN_STATE_HEIGHT_PX * iRows);
        settings.addCallback(layoutRequestCallback);

        JQueryWrapper.select(this).animate(VISIBILITY_TOGGLE_SPEED, settings);
    }
}
