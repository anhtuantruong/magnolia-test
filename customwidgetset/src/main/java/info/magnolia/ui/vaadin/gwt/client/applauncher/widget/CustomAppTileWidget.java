package info.magnolia.ui.vaadin.gwt.client.applauncher.widget;

import com.google.gwt.dom.client.Style;
import com.google.web.bindery.event.shared.EventBus;
import info.magnolia.ui.vaadin.gwt.client.applauncher.shared.AppTile;

/**
 * Created by tuan on 5/11/17.
 */
public class CustomAppTileWidget extends AppTileWidget {

    private boolean clientGroup;


    public CustomAppTileWidget(EventBus eventBus, AppTile appTile, boolean clientGroup) {
        super(eventBus, appTile);
        this.clientGroup = clientGroup;
    }

    @Override
    public void updateColors() {
        setColors(isActive());
    }

    /**
     * Set colors with the group coloring.
     */
    private void setColors(boolean isTileWhite) {
        final Style style = getElement().getStyle();
        if (isTileWhite) {
            style.setColor(getParentColor());
            style.setBackgroundColor("white");
        } else {
            style.setBackgroundColor(getParentColor());
            style.setColor("white");
        }
    }

    private String getParentColor() {
        return getParent().getColor();
    }
}
