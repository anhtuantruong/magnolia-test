package info.magnolia.ui.vaadin.gwt.client.applauncher.connector;

import com.google.gwt.dom.client.Element;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Widget;
import com.google.web.bindery.event.shared.EventBus;
import com.google.web.bindery.event.shared.SimpleEventBus;
import com.vaadin.client.communication.StateChangeEvent;
import com.vaadin.client.communication.StateChangeEvent.StateChangeHandler;
import com.vaadin.client.ui.AbstractComponentConnector;
import com.vaadin.client.ui.layout.ElementResizeListener;
import com.vaadin.shared.ui.Connect;
import info.magnolia.ui.vaadin.applauncher.CustomAppLauncher;
import info.magnolia.ui.vaadin.gwt.client.applauncher.shared.AppGroup;
import info.magnolia.ui.vaadin.gwt.client.applauncher.shared.AppTile;
import info.magnolia.ui.vaadin.gwt.client.applauncher.widget.AppLauncherView;
import info.magnolia.ui.vaadin.gwt.client.applauncher.widget.CustomAppLauncherView;
import info.magnolia.ui.vaadin.gwt.client.applauncher.widget.CustomAppLauncherViewImpl;

import java.util.*;

/**
 * Created by tuan on 5/11/17.
 */

/**
 * Client-side connector for {@link CustomAppLauncher} component.
 * Copy from {@link AppLauncherConnector} version 5.4.8
 */
@Connect(CustomAppLauncher.class)
public class CustomAppLauncherConnector extends AbstractComponentConnector implements AppLauncherView.Presenter {

    private CustomAppLauncherView view;

    private EventBus eventBus = new SimpleEventBus();

    private HandlerRegistration eventPreviewHandle;

    private Map<Element, ElementResizeListener> resizeListeners = new HashMap<Element, ElementResizeListener>();

    public CustomAppLauncherConnector() {

        addStateChangeHandler("appGroups", new StateChangeHandler() {
            @Override
            public void onStateChanged(StateChangeEvent stateChangeEvent) {
                // set groups in specified order
                List<AppGroup> groups = new ArrayList<AppGroup>(getState().appGroups.values());
                Collections.sort(groups, new GroupComparator(getState().groupsOrder));

                // add groups to the view
                view.clear();
                view.setDescriptionText(getState().descriptionText);
                for (final AppGroup appGroup : groups) {
                    view.addAppGroup(appGroup);
                    for (final AppTile tile : appGroup.getAppTiles()) {
                        view.addAppTile(tile, appGroup);
                    }
                }

                updateRunningAppTiles();
            }
        });

        addStateChangeHandler("runningApps", new StateChangeHandler() {
            @Override
            public void onStateChanged(StateChangeEvent stateChangeEvent) {
                updateRunningAppTiles();
            }
        });
    }

    @Override
    protected void init() {
        super.init();
        this.eventPreviewHandle = Event.addNativePreviewHandler(new Event.NativePreviewHandler() {
            @Override
            public void onPreviewNativeEvent(Event.NativePreviewEvent event) {
                int eventCode = event.getTypeInt();
                boolean isTouchOrMouse = (eventCode & Event.MOUSEEVENTS) > 0 || (eventCode & Event.TOUCHEVENTS) > 0;

                final Element eventTarget = event.getNativeEvent().getEventTarget().cast();

                if (!getWidget().getElement().isOrHasChild(eventTarget)) {
                    return;
                }

                if ((isTouchOrMouse && eventCode != Event.ONMOUSEOVER && eventCode != Event.ONMOUSEOUT && getConnection().hasActiveRequest())) {
                    event.cancel();
                }
            }
        });
    }

    private void updateRunningAppTiles() {
        for (final AppGroup appGroup : getState().appGroups.values()) {
            for (final AppTile tile : appGroup.getAppTiles()) {
                if (getState().runningApps.contains(tile.getName())) {
                    view.setAppActive(tile.getName(), true);
                } else {
                    view.setAppActive(tile.getName(), false);
                }
            }
        }
    }

    @Override
    public Widget getWidget() {
        return super.getWidget();
    }

    @Override
    protected Widget createWidget() {
        this.view = new CustomAppLauncherViewImpl(eventBus);
        this.view.setPresenter(this);
        return view.asWidget();
    }

    @Override
    public CustomAppLauncherState getState() {
        return (CustomAppLauncherState) super.getState();
    }

    @Override
    protected CustomAppLauncherState createState() {
        return new CustomAppLauncherState();
    }

    @Override
    public void onUnregister() {
        super.onUnregister();
        eventPreviewHandle.removeHandler();
        final Iterator<Map.Entry<Element,ElementResizeListener>> it = resizeListeners.entrySet().iterator();
        while (it.hasNext()) {
            final Map.Entry<Element,ElementResizeListener> entry = it.next();
            getLayoutManager().removeElementResizeListener(entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void activateApp(String appName) {
        History.newItem("app:" + appName, true);
    }

    @Override
    public void registerElementResizeListener(Element element, ElementResizeListener elementResizeListener) {
        resizeListeners.put(element, elementResizeListener);
        getLayoutManager().addElementResizeListener(element, elementResizeListener);
    }

    /**
     * This comparator helps ordering the list of groups according to the specified groupsOrder, yet accounting as well for ordering all permanent groups
     * before all temporary groups. This ensures accurate relative depth of groups in the applauncher.
     */
    static class GroupComparator implements Comparator<AppGroup> {

        private List<String> groupOrder;

        public GroupComparator(List<String> groupOrder) {
            this.groupOrder = groupOrder;
        }

        @Override
        public int compare(AppGroup o1, AppGroup o2) {
            boolean perm1 = o1.isPermanent();
            boolean perm2 = o2.isPermanent();
            if (perm1 == perm2) {
                Integer idx1 = groupOrder.indexOf(o1.getName());
                Integer idx2 = groupOrder.indexOf(o2.getName());
                return idx1.compareTo(idx2);
            }
            return perm1 == true ? -1 : 1;
        }
    }

}
