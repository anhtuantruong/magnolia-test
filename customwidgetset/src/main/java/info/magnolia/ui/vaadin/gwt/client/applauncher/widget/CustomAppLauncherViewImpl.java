package info.magnolia.ui.vaadin.gwt.client.applauncher.widget;

/**
 * Created by tuan on 5/11/17.
 */

import com.google.gwt.dom.client.Style;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ScrollEvent;
import com.google.gwt.event.dom.client.ScrollHandler;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.web.bindery.event.shared.EventBus;
import com.vaadin.client.ComputedStyle;
import com.vaadin.client.ui.layout.ElementResizeEvent;
import com.vaadin.client.ui.layout.ElementResizeListener;
import info.magnolia.ui.vaadin.gwt.client.applauncher.event.AppActivationEvent;
import info.magnolia.ui.vaadin.gwt.client.applauncher.shared.AppGroup;
import info.magnolia.ui.vaadin.gwt.client.applauncher.shared.AppTile;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Copy from {@link AppLauncherViewImpl} version 5.4.8
 */
public class CustomAppLauncherViewImpl extends FlowPanel implements CustomAppLauncherView, AppActivationEvent.Handler {

    private static final double TEMPORARY_PERMANENT_SECTIONS_OFFSET = 170.0;

    private static final String PERMANENT_APP_GROUP_BORDER = "permanent-app-group-border";

    private static final String PERMANENT_APP_GROUP_BORDER_BOTTOM = PERMANENT_APP_GROUP_BORDER + "-bottom";

    private static final String PERMANENT_APP_GROUP_BORDER_TOP = PERMANENT_APP_GROUP_BORDER + "-top";

    public static final String PERMANENT_APP_SCROLL_PANEL = "permanent-app-scroll-panel";

    // Adding for custom in GALERIE-1861
    public static final String PERMANENT_DESCRIPTION_PANEL = "permanent-description-panel";
    private final CustomVTemporaryAppGroupBar temporarySectionsBar = new CustomVTemporaryAppGroupBar();
    private final Label descriptionLabel = new Label();

    private final Map<String, VAppTileGroup> groups = new HashMap<String, VAppTileGroup>();

    private final FlowPanel descriptionBar = new FlowPanel();

    private final EventBus eventBus;

    private Presenter presenter;

    private ScrollPanel permanentAppScrollPanel = new ScrollPanel();;

    private FlowPanel permanentAppContainer = new FlowPanel();;

    private ComputedStyle permanentAppScrollPanelStyle = new ComputedStyle(permanentAppScrollPanel.getElement());

    public CustomAppLauncherViewImpl(final EventBus eventBus) {
        super();
        this.eventBus = eventBus;
        descriptionBar.addStyleName(PERMANENT_DESCRIPTION_PANEL);
        descriptionBar.add(descriptionLabel);
        add(descriptionBar);
        add(temporarySectionsBar);
        this.eventBus.addHandler(AppActivationEvent.TYPE, this);

        permanentAppScrollPanel.getElement().getStyle().setOverflowX(Style.Overflow.HIDDEN);
        permanentAppScrollPanel.addStyleName(PERMANENT_APP_SCROLL_PANEL);
        permanentAppScrollPanel.getElement().getStyle().setPosition(Position.ABSOLUTE);

        permanentAppScrollPanel.add(permanentAppContainer);
        permanentAppScrollPanel.addScrollHandler(new ScrollHandler() {
            @Override
            public void onScroll(ScrollEvent event) {
                updatePermanentAppGroupBorder();
            }
        });
        add(permanentAppScrollPanel);
    }

    @Override
    public void addAppGroup(AppGroup group) {
        if (group.isPermanent()) {
            addPermanentAppGroup(group);
        } else {
            addTemporaryAppGroup(group);
        }
    }

    public void addTemporaryAppGroup(AppGroup groupParams) {
        final CustomVTemporaryAppTileGroup group = new CustomVTemporaryAppTileGroup(groupParams.getBackgroundColor());
        group.setClientGroup(groupParams.isClientGroup());
        groups.put(groupParams.getName(), group);
        temporarySectionsBar.addGroup(groupParams.getCaption(), group);
        add(group);

        presenter.registerElementResizeListener(group.getElement(), new ElementResizeListener() {
            @Override
            public void onElementResize(ElementResizeEvent e) {
                final CustomVTemporaryAppTileGroup currentOpenGroup = temporarySectionsBar.getCurrentOpenGroup();
                if (currentOpenGroup == null || group == currentOpenGroup) {
                    permanentAppScrollPanel.getElement().getStyle().setBottom(TEMPORARY_PERMANENT_SECTIONS_OFFSET + group.getOffsetHeight(), Unit.PX);
                    updatePermanentAppGroupBorder();
                }
            }
        });
    }

    public void addPermanentAppGroup(AppGroup groupParams) {
        final CustomVPermanentAppTileGroup group = new CustomVPermanentAppTileGroup(groupParams.getCaption(), groupParams.getBackgroundColor());
        group.setClientGroup(groupParams.isClientGroup());
        groups.put(groupParams.getName(), group);
        permanentAppContainer.add(group);

        presenter.registerElementResizeListener(permanentAppScrollPanel.getElement(), new ElementResizeListener() {
            @Override
            public void onElementResize(ElementResizeEvent e) {
                updatePermanentAppGroupBorder();
            }
        });
    }

    private void updatePermanentAppGroupBorder() {
        permanentAppScrollPanel.removeStyleName(PERMANENT_APP_GROUP_BORDER_BOTTOM);
        permanentAppScrollPanel.removeStyleName(PERMANENT_APP_GROUP_BORDER_TOP);
        int maxScrollPosition = getPermanentAppGroupMaxScrollTop();
        if (maxScrollPosition > 0) {
            int verticalScrollPosition = permanentAppScrollPanel.getVerticalScrollPosition();
            if (verticalScrollPosition > 0) {
                permanentAppScrollPanel.addStyleName(PERMANENT_APP_GROUP_BORDER_TOP);
            }

            if (verticalScrollPosition < maxScrollPosition) {
                permanentAppScrollPanel.addStyleName(PERMANENT_APP_GROUP_BORDER_BOTTOM);
            }

        }
    }

    private int getPermanentAppGroupMaxScrollTop() {
        return permanentAppScrollPanel.getMaximumVerticalScrollPosition() - permanentAppScrollPanelStyle.getBorder()[0] - permanentAppScrollPanelStyle.getBorder()[2];
    }

    @Override
    public void onAppActivated(AppActivationEvent event) {
        presenter.activateApp(event.getAppName());
    }

    @Override
    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setAppActive(String appName, boolean isActive) {
        for (Entry<String, VAppTileGroup> entry : groups.entrySet()) {
            if (entry.getValue().hasApp(appName)) {
                AppTileWidget tile = entry.getValue().getAppTile(appName);
                tile.setActiveState(isActive);
            }
        }
    }

    @Override
    public void addAppTile(AppTile tileData, AppGroup groupData) {
        CustomAppTileWidget tile = new CustomAppTileWidget(eventBus, tileData, groupData.isClientGroup());
        VAppTileGroup group = groups.get(groupData.getName());
        if (group != null) {
            group.addAppTile(tile);
        }
    }

    @Override
    public void clear() {
        temporarySectionsBar.clear();
        for (final VAppTileGroup group : groups.values()) {
            remove(group);
        }
        groups.clear();
    }

    @Override
    public void setDescriptionText(String descriptionText) {
        descriptionLabel.setText(descriptionText);
    }
}
