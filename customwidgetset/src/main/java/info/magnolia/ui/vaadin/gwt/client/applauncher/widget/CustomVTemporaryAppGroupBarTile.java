package info.magnolia.ui.vaadin.gwt.client.applauncher.widget;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.FlowPanel;
import com.googlecode.mgwt.dom.client.event.touch.TouchEndEvent;
import com.googlecode.mgwt.dom.client.event.touch.TouchEndHandler;
import com.googlecode.mgwt.dom.client.event.touch.TouchStartEvent;
import com.googlecode.mgwt.dom.client.event.touch.TouchStartHandler;
import com.googlecode.mgwt.ui.client.widget.touch.TouchDelegate;

/**
 * Created by tuan on 5/11/17.
 * Copy from {@link VTemporaryAppGroupBarTile}  version 5.4.8
 */
public class CustomVTemporaryAppGroupBarTile extends FlowPanel {

    private Element element;
    private CustomVTemporaryAppGroupBar groupBar;
    private VAppTileGroup group;
    private CustomVTemporaryAppGroupBarTile that;
    private boolean opened;

    public CustomVTemporaryAppGroupBarTile(String caption, VAppTileGroup groupParam, CustomVTemporaryAppGroupBar groupBarParam) {
        super();
        groupBar = groupBarParam;
        group = groupParam;
        that = this;

        if (group instanceof CustomVTemporaryAppTileGroup) {
            constructDOM(caption);
            bindHandlers();
        }
    }

    private void constructDOM(String caption) {

        element = this.getElement();
        element.addClassName("item");
        element.addClassName("section");
        element.addClassName("closed");

        /*
         * if (group.isClientGroup()) {
         * element.addClassName("client-group");
         * } else {
         * }
         */

        final Element label = DOM.createSpan();
        label.addClassName("sectionLabel");
        if (caption != null && !caption.contains("\u00AD")) {
            label.addClassName("wordwrap");
        }
        label.setInnerText(caption);

        Element notch = DOM.createSpan();
        notch.addClassName("notch");

        element.appendChild(label);
        element.appendChild(notch);

    }

    private void bindHandlers() {

        DOM.sinkEvents(element, Event.MOUSEEVENTS);
        addDomHandler(new MouseOverHandler() {
            @Override
            public void onMouseOver(MouseOverEvent event) {
                getElement().addClassName("hover");
            }
        }, MouseOverEvent.getType());

        addDomHandler(new MouseOutHandler() {
            @Override
            public void onMouseOut(MouseOutEvent event) {
                getElement().removeClassName("hover");
                updateColors();
            }
        }, MouseOutEvent.getType());

        final TouchDelegate touchDelegate = new TouchDelegate(this);

        touchDelegate.addTouchStartHandler(new TouchStartHandler() {

            @Override
            public void onTouchStart(TouchStartEvent event) {
                getElement().removeClassName("hover");
                setColorsClick();
                event.preventDefault();
            }
        });

        touchDelegate.addTouchEndHandler(new TouchEndHandler() {

            @Override
            public void onTouchEnd(TouchEndEvent event) {
                getElement().removeClassName("hover");
                groupBar.handleTileClick((CustomVTemporaryAppTileGroup) group, that);
                updateColors();
                event.preventDefault();
            }
        });

    }

    private void setColorsClick() {
        final Style style = getElement().getStyle();
        if (!group.isClientGroup()) {
            style.setColor(group.getColor());
            style.setBorderColor("white");
        } else {
            style.setBorderColor(group.getColor());
            style.setColor("white");
        }
    }

    private void updateColors() {
        if (opened) {
            if (group.isClientGroup()) {
                element.getStyle().setColor(group.getColor());
                element.getStyle().setBorderColor("white");
            } else {
                element.getStyle().setColor("white");
                element.getStyle().setBorderColor(group.getColor());
            }
        } else {
            element.getStyle().clearBorderColor();
            element.getStyle().clearColor();
        }
    }

    public void openExpander() {
        element.removeClassName("hover");
        element.removeClassName("closed");
        element.addClassName("open");

        opened = true;
        updateColors();
    }

    public void closeExpander() {
        element.removeClassName("hover");
        element.removeClassName("open");
        element.addClassName("closed");

        opened = false;
        updateColors();
    }
}
