package info.magnolia.ui.vaadin.gwt.client.applauncher.widget;

/**
 * Created by tuan on 5/12/17.
 */
public interface CustomAppLauncherView extends AppLauncherView {
    void setDescriptionText(String descriptionText);
}
