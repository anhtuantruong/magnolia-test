package info.magnolia.ui.vaadin.gwt.client.applauncher.widget;

import info.magnolia.ui.vaadin.gwt.client.applauncher.connector.CustomAppLauncherConnector;
import com.vaadin.client.Util;
import info.magnolia.ui.vaadin.gwt.client.jquerywrapper.JQueryWrapper;

/**
 * Created by tuan on 5/11/17.
 * Copy from {@link AppLauncherUtil} version 5.4.8
 */
public class CustomAppLauncherUtil {

    public static CustomAppLauncherConnector getConnector() {
        return (CustomAppLauncherConnector) Util.findConnectorFor(getView().asWidget());
    }

    public static CustomAppLauncherView getView() {
        return Util.findWidget(JQueryWrapper.select(".v-app-launcher").get(0), CustomAppLauncherViewImpl.class);
    }
}
