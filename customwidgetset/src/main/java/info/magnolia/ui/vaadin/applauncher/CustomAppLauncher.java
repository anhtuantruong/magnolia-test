package info.magnolia.ui.vaadin.applauncher;

import com.vaadin.ui.AbstractComponent;
import info.magnolia.ui.vaadin.gwt.client.applauncher.connector.CustomAppLauncherState;
import info.magnolia.ui.vaadin.gwt.client.applauncher.shared.AppGroup;
import info.magnolia.ui.vaadin.gwt.client.applauncher.shared.AppTile;

import java.util.List;

/**
 * Server side of CustomAppLauncher.
 * Copy from {@link AppLauncher} version 5.4.8
 */
public class CustomAppLauncher extends AbstractComponent {

    public CustomAppLauncher() {
        super();
        setImmediate(true);
        addStyleName("v-app-launcher");
    }

    public void addAppGroup(String name, String caption, String color, boolean isPermanent, boolean clientGroup) {
        getState().appGroups.put(name, new AppGroup(name, caption, color, isPermanent, clientGroup));
        getState().groupsOrder.add(name);
    }

    @Override
    protected CustomAppLauncherState getState() {
        return (CustomAppLauncherState) super.getState();
    }

    public void addAppTile(String name, String caption, String icon, String groupName) {
        final AppGroup group = getState().appGroups.get(groupName);
        if (group != null) {
            group.addAppTile(new AppTile(name, caption, icon));
        }
    }

    public void clear() {
        getState().groupsOrder.clear();
        getState().appGroups.clear();
        getState().runningApps.clear();
    }

    public void setDescriptionText(String descriptionText) {
        getState().descriptionText = descriptionText;
    }

    public void setAppActive(String appName, boolean isActive) {
        final List<String> runningApps = getState().runningApps;
        if (isActive && !runningApps.contains(appName)) {
            runningApps.add(appName);
        } else {
            runningApps.remove(appName);
        }
    }
}
