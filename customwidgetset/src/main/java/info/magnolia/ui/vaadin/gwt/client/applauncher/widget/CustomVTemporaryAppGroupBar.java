package info.magnolia.ui.vaadin.gwt.client.applauncher.widget;

import com.google.gwt.user.client.ui.FlowPanel;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tuan on 5/11/17.
 * Copy from {@link VTemporaryAppGroupBar} version 5.4.8
 */
public class CustomVTemporaryAppGroupBar extends FlowPanel {

    private final Map<CustomVTemporaryAppGroupBarTile, CustomVTemporaryAppTileGroup> groupMap = new HashMap<CustomVTemporaryAppGroupBarTile, CustomVTemporaryAppTileGroup>();

    private CustomVTemporaryAppTileGroup currentOpenGroup = null;

    public CustomVTemporaryAppGroupBar() {
        super();
        addStyleName("app-list");
        addStyleName("sections");
    }

    public CustomVTemporaryAppTileGroup getCurrentOpenGroup() {
        return currentOpenGroup;
    }

    public void addGroup(String caption, VAppTileGroup group) {
        CustomVTemporaryAppGroupBarTile groupTile = new CustomVTemporaryAppGroupBarTile(caption, group, this);
        groupMap.put(groupTile, (CustomVTemporaryAppTileGroup) group);
        add(groupTile);
    }

    protected void handleTileClick(CustomVTemporaryAppTileGroup group, CustomVTemporaryAppGroupBarTile groupTile) {
        closeCurrentOpenExpander();
        if (group != null) {
            if (currentOpenGroup != group) {
                if (currentOpenGroup != null) {
                    currentOpenGroup.closeSection();
                }
                group.showSection();
                currentOpenGroup = group;
                groupTile.openExpander();
            } else {
                currentOpenGroup.closeSection();
                currentOpenGroup = null;
            }
        }
    }

    protected void closeCurrentOpenExpander() {
        if (currentOpenGroup != null) {
            for (Map.Entry<CustomVTemporaryAppGroupBarTile, CustomVTemporaryAppTileGroup> entry : groupMap.entrySet()) {
                if (currentOpenGroup == entry.getValue()) {
                    entry.getKey().closeExpander();
                    break;
                }
            }
        }
    }
}
