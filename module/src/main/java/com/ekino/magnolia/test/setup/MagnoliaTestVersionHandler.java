package com.ekino.magnolia.test.setup;

import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.delta.*;

import com.google.common.collect.Lists;
import info.magnolia.cms.security.MgnlKeyPair;
import info.magnolia.cms.security.Permission;
import info.magnolia.cms.security.SecurityUtil;
import info.magnolia.init.MagnoliaConfigurationProperties;
import info.magnolia.module.DefaultModuleVersionHandler;
import info.magnolia.module.InstallContext;
import info.magnolia.module.delta.*;
import info.magnolia.rendering.module.setup.InstallRendererContextAttributeTask;

import javax.inject.Inject;
import javax.jcr.ImportUUIDBehavior;
import java.util.ArrayList;
import java.util.List;

import static info.magnolia.cms.security.UserManager.ANONYMOUS_USER;
import static info.magnolia.cms.security.UserManager.SYSTEM_USER;

/**
 * This class is optional and lets you manage the versions of your module, by registering "deltas" to maintain the
 * module's configuration, or other type of content. If you don't need this, simply remove the reference to this class
 * in the module descriptor xml.
 *
 * @see info.magnolia.module.DefaultModuleVersionHandler
 * @see info.magnolia.module.ModuleVersionHandler
 * @see info.magnolia.module.delta.Task
 */
public class MagnoliaTestVersionHandler extends DefaultModuleVersionHandler {

    private static final String PRIVATE_KEY = "30820278020100300D06092A864886F70D0101010500048202623082025E02010002818100CA049B6862D6146426469C7E5A802E1085C62B7D8866B79F5FFC13538A269E7C687C4214F68BE95644904A72CAD25B5F3C5D3B04AE63EC121B864C7705707B3BFFD9E546EF8CC3403BE0893147F5BDCD6B1C1DE3446D2CC8F6CBE746116F75E454913421313FBC1A3FD3B97A1F4C11CA93994290DC562E74F75902EE49B7141F020301000102818100AC5951DCB4B5BF9FD1A7DC4B501A9B322D6EEC1BA50B3D0D136E936E6A26987FBB0487BC2ABB22CF1E9F9B6463F5234F14C8D559210ECE90843B0771638A9B1706DF58182DF30D86F3A5D81AEC5389A2F62FBF509390598A2F1D045AAAF5BB12FAFBBACA656A1CC8311E2E98300BD05E36AA6EC6795DF83878EC5A7B503D6641024100FD43D19F29B8076F19E8D2EB19FA2A5233F2435411A16125A4CC6ED0F1C3DA1FD29A4A73516999BD272C8A0969ABD65F0F91014B7E04FD1618CBC45B3636BC75024100CC331C27CCCFF4BAAC79FBF05FCE20FC681FC16E2EB9DC4AE51A8602FDAD4409B56F23ABE53CA61229BAC87BD074E5EB2C15451CE48AC42FCBBD2D1AA0AA8BC3024100F82206F18ABD3BC3553E2FCF997EE911EAC513C3FD16008912221CFC67B23C35EB6FB13BB46AE2EE58C02169C177532B85A3CB01FCB0C4896EB7CD64940422B5024063C5512B6D19EA744F1366F2A28CDF771414F7F8B2E1E2EDC114FD877694F2DA5F88C7001508560827D578616E8FBF4AF1860EF7B7FEC92EA5387052435030EB024100C528CDCD950F4F1942F0AAB6642AFE00D3043D8D987814B50FB4194D99AA8BC208F8EDDEF19D302B7169D3BA970772EAAC06A045BCE77E8A0C8854B8E02FA129";
    private static final String PUBLIC_KEY = "30819F300D06092A864886F70D010101050003818D0030818902818100CA049B6862D6146426469C7E5A802E1085C62B7D8866B79F5FFC13538A269E7C687C4214F68BE95644904A72CAD25B5F3C5D3B04AE63EC121B864C7705707B3BFFD9E546EF8CC3403BE0893147F5BDCD6B1C1DE3446D2CC8F6CBE746116F75E454913421313FBC1A3FD3B97A1F4C11CA93994290DC562E74F75902EE49B7141F0203010001";

    private MagnoliaConfigurationProperties configurationProperties;

    private boolean isAuthorInstance;

    @Inject
    public MagnoliaTestVersionHandler(MagnoliaConfigurationProperties configurationProperties) {
        this.configurationProperties = configurationProperties;

        this.isAuthorInstance = Boolean.parseBoolean(configurationProperties.getProperty("magnolia.bootstrap.authorInstance"));
    }

    @Override
    protected List<Task> getStartupTasks(InstallContext installContext) {
        List<Task> tasks = new ArrayList<>();
        tasks.addAll(super.getStartupTasks(installContext));

        /*
        In CE edition of Magnolia, there is only on subscriber
        so that we can set the subscriber info directly in the default one
          */
        if (isAuthorInstance) {
            // author only tasks
            tasks.add(new SetPropertyTask("config", "/server/activation/subscribers/magnoliaPublic8080", "URL",
                    configurationProperties.getProperty("public.base.url")));
            SecurityUtil.updateKeys(new MgnlKeyPair(PRIVATE_KEY, PUBLIC_KEY));
        } else {
            // public only tasks
            SecurityUtil.updateKeys(new MgnlKeyPair(null, PUBLIC_KEY));

            tasks.add(new SetPropertyTask("config", "/modules/ui-admincentral/virtualURIMapping/default",
                    "toURI", "/index"));
        }

        tasks.add(new NodeExistsDelegateTask("remove site travel", "Install site travel description",
                "config", "/modules/multisite/config/sites/travel",
                new RemoveNodeTask("remove site sportstation", "/modules/multisite/config/sites/travel"), null));
        // remove theme sportstation and travel
        tasks.add(new NodeExistsDelegateTask("remove theme sportstation", "remove theme sportstation",
                "config", "/modules/site/config/themes/sportstation-theme",
                new RemoveNodeTask("remove theme sportstation", "/modules/site/config/themes/sportstation-theme"), null));

        tasks.add(new NodeExistsDelegateTask("remove theme travel", "remove theme travel",
                "config", "/modules/site/config/themes/travel-demo-theme",
                new RemoveNodeTask("remove theme travel", "/modules/site/config/themes/travel-demo-theme"), null));

        tasks.add(new NodeExistsDelegateTask("remove page travel", "remove page travel", "website",
                "/travel", new RemoveNodeTask("remove page travel", "remove page travel", "website", "/travel"),
                null));

        tasks.add(new NodeExistsDelegateTask("remove dam travel-demo", "remove dam travel-demo",
                "dam", "/travel-demo", new RemoveNodeTask("remove dam travel-demo",
                "remove dam travel-demo", "dam", "/travel-demo"),
                null));

        return tasks;
    }

}