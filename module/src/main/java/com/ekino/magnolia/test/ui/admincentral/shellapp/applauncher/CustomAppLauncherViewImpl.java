package com.ekino.magnolia.test.ui.admincentral.shellapp.applauncher;

import com.vaadin.ui.Component;
import info.magnolia.cms.security.User;
import info.magnolia.cms.security.operations.AccessDefinition;
import info.magnolia.context.Context;
import info.magnolia.i18nsystem.SimpleTranslator;
import info.magnolia.ui.admincentral.shellapp.applauncher.AppLauncherView;
import info.magnolia.ui.api.app.AppDescriptor;
import info.magnolia.ui.api.app.launcherlayout.AppLauncherGroup;
import info.magnolia.ui.api.app.launcherlayout.AppLauncherGroupEntry;
import info.magnolia.ui.api.app.launcherlayout.AppLauncherLayout;
import info.magnolia.ui.vaadin.applauncher.CustomAppLauncher;

import javax.inject.Inject;

/**
 * Created by tuan on 5/11/17.
 */
public class CustomAppLauncherViewImpl implements AppLauncherView {

    private final CustomAppLauncher appLauncher = new CustomAppLauncher();

    private Presenter presenter;

    @Inject
    SimpleTranslator i18n;

    private final Context context;

    @Inject
    public CustomAppLauncherViewImpl(final Context context) {
        this.context = context;
    }

    public Presenter getPresenter() {
        return presenter;
    }

    @Override
    public void setPresenter(Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public Component asVaadinComponent() {
        return appLauncher;
    }

    @Override
    public void activateButton(boolean activate, String appName) {
        appLauncher.setAppActive(appName, activate);
    }

    @Override
    public void clearView() {
        appLauncher.clear();
    }

    @Override
    public void registerApp(AppLauncherLayout layout) {
        appLauncher.setDescriptionText(i18n.translate("dashboard.description.text"));
        for (AppLauncherGroup group : layout.getGroups()) {
            appLauncher.addAppGroup(group.getName(), group.getLabel(), group.getColor(), group.isPermanent(), group.isClientGroup());
            for (AppLauncherGroupEntry entry : group.getApps()) {
                AppDescriptor descriptor = entry.getAppDescriptor();

            }
        }
    }


}
